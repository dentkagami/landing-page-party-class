import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem } from 'reactstrap';
import lg1 from '../assets/ImgLT/landingpage/Vector.svg'
import lg2 from '../assets/ImgLT/landingpage/logo-instagram.svg'
import lg3 from '../assets/ImgLT/landingpage/logo-twitter.svg'
import lg4 from '../assets/ImgLT/landingpage/logo-youtube.svg'
import { Jumbotron } from 'reactstrap';
import appstore from '../assets/ImgLT/SVG/appstore.svg';
import { CardTitle, CardSubtitle } from 'reactstrap';
import branded from '../assets/ImgLT/landingpage/brandtest.svg';



export default class AppHeader extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <div className='respon'>
        <Navbar className="footer" expand="md">
        <NavbarBrand className="brand" href=""><img src={branded} width={60} height={60} alt='bebas'></img>LET'S TALK</NavbarBrand>                   
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
          <Nav className="footer1" navbar>
              <NavItem>
                <Navbar className='c'>Find Us:</Navbar>
              </NavItem>                    
            
              <NavItem>
              <div className='group'>
              <NavbarBrand className="badge" href="www.apple.com">
              <img className='fb'src={lg1}alt='group1'></img>
              </NavbarBrand>
              </div>
              </NavItem>       
            
              <NavItem>
              <div className='group'>
              <NavbarBrand className="badge" href="www.apple.com">
              <img src={lg2}alt='group2'></img>
              </NavbarBrand>
              </div>              
              </NavItem>
            
              <NavItem>
              <div className='group'>
              <NavbarBrand className="badge" href="www.apple.com">
              <img src={lg3}alt='group3'></img>
              </NavbarBrand>
              </div>              
              </NavItem>

              <NavItem>
              <div className='group'>
              <NavbarBrand className="badge" href="www.apple.com">
              <img src={lg4}alt='group3'></img>
              </NavbarBrand>
              </div>              
              </NavItem>
            </Nav>
          </Collapse>          
        </Navbar>
        
        <Jumbotron className='inti'>
        <p className="lead"></p>        
        <p className='alamat'>Let’s Talk is an online platform that makes everyone can speak up about their problem or their feelings.</p>
        <p className="alamat2">Jl. Raya Damai No. 7 Serpong, Tangerang Selatan</p>

        <div className='button11'>
        <NavbarBrand className="badge" href="www.apple.com">
        <img src={appstore} width={161} height={48} alt='group1'></img>
        </NavbarBrand>
        </div>

        <div className='profil'>
        <CardTitle className='judulprofil'>Learn More</CardTitle>
        <CardSubtitle>About Us Contact Us FAQ</CardSubtitle>
        </div>

        <div className='profil2'>
        <CardTitle className='judulprofil'>Connect</CardTitle>
        <CardSubtitle>Our Team</CardSubtitle>
        </div>

               
      </Jumbotron>
      </div>
    );
  }
}