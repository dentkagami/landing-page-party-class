import React from 'react';
import {
  Navbar,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink } from 'reactstrap';
import branded from '../assets/ImgLT/landingpage/brandtest.svg';
import { Link } from 'react-router-dom';

export default class AppHeader extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <div className='respon'>
        <Navbar className="test" expand="md">
        <NavbarBrand className="brand" href=""><img src={branded} width={60} height={60} alt='bebas'></img>LET'S TALK</NavbarBrand>                    
          <Nav className="aja" navbar>
              <NavItem>
                <NavLink className='aja1'><Link to="/">Home</Link></NavLink>
              </NavItem>                    
              <NavItem>
              <Link to="aboutus">
              <NavLink className='aja2'>About Us</NavLink>
              </Link>
              </NavItem>                  
            </Nav>
         
        </Navbar>
      </div>
    );
  }
}