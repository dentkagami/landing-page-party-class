import React from 'react';
import { Navbar, NavbarBrand, Nav, NavItem, NavLink, Collapse, NavbarToggler } from 'reactstrap';
import branded from '../assets/ImgLT/landingpage/brandtest.svg';
import { CardTitle, CardSubtitle, Jumbotron } from 'reactstrap';
import about1 from '../assets/ImgLT/landingpage/couple.png';
import about2 from '../assets/ImgLT/landingpage/udel.png';
import about3 from '../assets/ImgLT/landingpage/buban.svg';
import about4 from '../assets/ImgLT/landingpage/backgronpeople.svg';
import lg1 from '../assets/ImgLT/landingpage/Vector.svg'
import lg2 from '../assets/ImgLT/landingpage/logo-instagram.svg'
import lg3 from '../assets/ImgLT/landingpage/logo-twitter.svg'
import lg4 from '../assets/ImgLT/landingpage/logo-youtube.svg'
import appstore from '../assets/ImgLT/SVG/appstore.svg';
import AppHeader from '../components/AppHeader';



export default class AboutUs extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <div className='aboutrespon'>

<AppHeader />

        {/* Body */}

        <div className='weapon'>
        <CardTitle className='judulabout'>STORY IS WEAPON</CardTitle>
        <CardSubtitle className='subabout'>
          Stories bring your good feeling and help people to grow up. Now stories are weapon to make word be better
        </CardSubtitle>
        </div>

        {/* Image */}
        <div className='aboutt2'>
        <img src={about2} width={400} height={600} alt='group1'></img>
        </div>

        <div className='aboutt1'>
        <img src={about1} width={586} height={627} alt='group1'></img>
        </div>

        {/* Speak Up */}
        <div className='weapon2'>
        <CardTitle className='judulabout2'>SPEAK UP</CardTitle>
        <CardSubtitle className='subabout2'>
        Share your story now, and make yourself feel relieved
        </CardSubtitle>
        </div>

      
      <Jumbotron className='jumbous'>
      
        <div className='aboutt4'>
        <img src={about4} width={1351} alt='group1'></img>
        </div>        
        <div className='aboutt3'>
        <img src={about3} width={381} height={250} alt='group1'></img>
        </div>
        <p className='tulisan'>LET’S TALK is an online platform that makes everyone can speak up about their problems or their feelings</p>
      </Jumbotron>

      {/* Footer */}
<div className='banban'>      
      <Navbar className="footer" expand="md">
      <NavbarBrand className="brand" href=""><img src={branded} width={60} height={60} alt='bebas'></img>LET'S TALK</NavbarBrand>                   
        <NavbarToggler onClick={this.toggle} />
        <Collapse isOpen={this.state.isOpen} navbar>
        <Nav className="footer1" navbar>
            <NavItem>
              <Navbar className='c'>Find Us:</Navbar>
            </NavItem>                    
          
            <NavItem>
            <div className='group'>
            <NavbarBrand className="badge" href="www.apple.com">
            <img className='fb'src={lg1}alt='group1'></img>
            </NavbarBrand>
            </div>
            </NavItem>       
          
            <NavItem>
            <div className='group'>
            <NavbarBrand className="badge" href="www.apple.com">
            <img src={lg2}alt='group2'></img>
            </NavbarBrand>
            </div>              
            </NavItem>
          
            <NavItem>
            <div className='group'>
            <NavbarBrand className="badge" href="www.apple.com">
            <img src={lg3}alt='group3'></img>
            </NavbarBrand>
            </div>              
            </NavItem>

            <NavItem>
            <div className='group'>
            <NavbarBrand className="badge" href="www.apple.com">
            <img src={lg4}alt='group3'></img>
            </NavbarBrand>
            </div>              
            </NavItem>
          </Nav>
        </Collapse>          
      </Navbar>
      
      <Jumbotron className='inti'>
      <p className="lead"></p>        
      <p className='alamat'>Let’s Talk is an online platform that makes everyone can speak up about their problem or their feelings.</p>
      <p className="alamat2">Jl. Raya Damai No. 7 Serpong, Tangerang Selatan</p>

      <div className='button11'>
      <NavbarBrand className="badge" href="www.apple.com">
      <img src={appstore} width={161} height={48} alt='group1'></img>
      </NavbarBrand>
      </div>

      <div className='profil'>
      <CardTitle className='judulprofil'>Learn More</CardTitle>
      <CardSubtitle>About Us Contact Us FAQ</CardSubtitle>
      </div>

      <div className='profil2'>
      <CardTitle className='judulprofil'>Connect</CardTitle>
      <CardSubtitle>Our Team</CardSubtitle>
      </div>

             
    </Jumbotron>
    </div>
    </div>

       

    );
  }
}
