import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppHeader from './components/AppHeader';
import AppJumbotron from './components/Jumbotron';
import '../src/assets/style/AppStyle.scss';
import AppProduct from './components/AppProduct';
import AppFooter from './components/Footer';
import Button from './components/Button';

class App extends Component {
  render() {
    return (
      <div>
        <AppHeader />
        <Button />
        <AppJumbotron />
        <AppProduct />
        <AppFooter />
      </div>
    );
  }
}

export default App;