import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import AboutUs from '../src/components/AboutUs';
import App from '../src/App';
import '../src/assets/style/AppStyle.scss';

export default class AppRouter extends React.Component{
    render(){
        return(
            <div>
                <BrowserRouter>
                <div>
                    <Route exact path='/' component={App}/>
                    <Route path='/aboutus' component={AboutUs}/>

                </div>
                </BrowserRouter>
            </div>
        );
    }
}